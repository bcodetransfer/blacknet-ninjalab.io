---
title: Migration to Tor addresses version 3
---

The first release of Blacknet shipped with support for Tor addresses v2.

The Tor Project has planned to retire it in 2021.[1]

The v3 update switches cryptographic algorithms and brings other improvements.
Cryptographic hash switched from SHA-1 to SHA-3.
Digital signature switched from RSA-1024 to Ed25519.
Key exchange switched from Diffie-Hellman to Elliptic-curve Diffie-Hellman.[2]

Address length changed from 16 characters to 56 characters.
For example, `zwdpzg2tzsqjinlb.onion` is version 2, `q4xgfyeusviqe3l25ogmgbcxc4c34laaqshurjtf6bxjub45mxumo2id.onion` is version 3.

Blacknet will start migration in version 0.3.

\[1\]: https://blog.torproject.org/v2-deprecation-timeline

\[2\]: https://blog.torproject.org/tor-0329-released-we-have-new-stable-series
