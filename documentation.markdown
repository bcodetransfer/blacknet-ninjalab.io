---
lastmod: 2020-11-14
title: Documentation
titleOnlyInHead: 1
---

## Developer

- [Cross-chain atomic swap](cross-chain-atomic-swap.html)
- [Message signing](message-signing.html)
- [Multisignature lock contract](multisignature-lock-contract.html)
- Peer-to-peer leasing
- [Token transfer](token-transfer.html)
- [Transaction batching](batching.html)
- [Trusted timestamping](trusted-timestamping.html)
- [Wallet and node RPC API](apiv2.html)

## Other

- [Proxy and anonymous P2P](proxy-and-anonymous-p2p.html)
- [Start staking](staking.html)
