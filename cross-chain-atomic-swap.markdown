---
title: Cross-chain atomic swap
---

Hash time lock contract or HTLC is a composition of a hash lock and a time lock.
The hash lock authorizes the ClaimHTLC transaction.
The time lock authorizes the RefundHTLC transaction.
The result of this contract is either success or refund, assuming that transactions are timely included to the blockchain.
It is also named an atomic swap.

## Hash lock

Hash lock is a cross-chain authorization mechanism that requires a preimage of a cryptographic hash.

- BLAKE2b-256
- SHA-256
- Keccak-256
- RIPEMD-160

## Time lock

Time lock is a cross-chain authorization mechanism that requires a specified block time or height to pass.

- Absolute block time
- Absolute block height
- Relative block time
- Relative block height

## RPC API

RPC API is not yet implemented.

- Create atomic swap |
--- | ---

- Clain atomic swap |
--- | ---

- Refund atomic swap |
--- | ---
