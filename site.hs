--------------------------------------------------------------------------------
--Copyright (c) 2018-2020 Blacknet Team
--Distributed under the MIT License
--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import           Data.Monoid (mappend)
import           Hakyll


--------------------------------------------------------------------------------
config :: Configuration
config = defaultConfiguration {
    destinationDirectory = "public"
}

feedConfig :: FeedConfiguration
feedConfig = FeedConfiguration
    { feedTitle       = "Blacknet"
    , feedDescription = "Blacknet archives"
    , feedAuthorName  = "Blacknet archives"
    , feedAuthorEmail = "archives@blacknet.ninja"
    , feedRoot        = "https://blacknet.ninja"
    }

main :: IO ()
main = hakyllWith config $ do
    match "robots.txt" $ do
        route   idRoute
        compile copyFileCompiler

    match "*.pdf" $ do
        route   idRoute
        compile copyFileCompiler

    match "js/*" $ do
        route   idRoute
        compile copyFileCompiler

    match "images/*" $ do
        route   idRoute
        compile copyFileCompiler

    match "fonts/*" $ do
        route   idRoute
        compile copyFileCompiler

    match "css/*" $ do
        route   idRoute
        compile compressCssCompiler

    match "*.markdown" $ do
        route   $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/default.html" pageCtx
            >>= relativizeUrls

    match "posts/*" $ do
        route $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/post.html"    postCtx
            >>= saveSnapshot "content"
            >>= loadAndApplyTemplate "templates/default.html" postCtx
            >>= relativizeUrls

    create ["archive.html"] $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let archiveCtx =
                    listField "posts" postCtx (return posts) `mappend`
                    constField "title" "Archives"            `mappend`
                    constField "additionalHeadTag" "<link rel=\"alternate\" type=\"application/atom+xml\" title=\"Blacknet\" href=\"/feed.atom\">" `mappend`
                    pageCtx

            makeItem ""
                >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
                >>= loadAndApplyTemplate "templates/default.html" archiveCtx
                >>= relativizeUrls

    match ("index.html" .||. "offline-wallet.html") $ do
        route   idRoute
        compile $ getResourceBody
            >>= loadAndApplyTemplate "templates/default.html" pageCtx
            >>= relativizeUrls

    match "specs.html" $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let indexCtx =
                    listField "posts" postCtx (return posts) `mappend`
                    pageCtx

            getResourceBody
                >>= applyAsTemplate indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls

    create ["feed.atom"] $ do
        route idRoute
        compile $ do
            let feedCtx = postCtx `mappend` bodyField "description"
            posts <- fmap (take 10) . recentFirst =<< loadAllSnapshots "posts/*" "content"
            renderAtom feedConfig feedCtx posts

    create ["sitemap.xml"] $ do
        route idRoute
        compile $ do
            pages <- loadAll ( "*.markdown"
                          .||. "index.html"
                          .||. "offline-wallet.html"
                          .||. "specs.html"
                          .||. redirected)
            posts <- recentFirst =<< loadAll "posts/*"
            let sitemapCtx =
                    listField "entries" pageCtx (return (pages ++ posts)) `mappend`
                    defaultContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/sitemap.xml" sitemapCtx

    match "templates/*" $ compile templateBodyCompiler

    match redirected $ do
        route   idRoute
        compile $ getResourceBody


--------------------------------------------------------------------------------
pageCtx :: Context String
pageCtx =
    constField "sitemapRoot" "https://blacknet.ninja" `mappend`
    defaultContext

postCtx :: Context String
postCtx =
    dateField "date" "%B %e, %Y" `mappend`
    pageCtx
--------------------------------------------------------------------------------
redirected =
      "anonnet.html"
 .||. "burn.html"
 .||. "docs.html"
 .||. "generator.html"
