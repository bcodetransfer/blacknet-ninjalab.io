---
lastmod: 2020-11-20
title: Transaction batching
---

In Blacknet, transactions can be combined into a Batch transaction that contains data for multiple transactions.
A batch is not permitted to contain another batch.
A batch is not permitted to contain only one transaction.
A batched transaction does not include Transaction Header saving 140 bytes and one signature verification.

A combination of Transfer transactions is also known as MassTransfer.
Such RPC API may be implemented in future.
The equivalent in Bitcoin is sendmany.

Remote batching for platform transaction of fungible token may be considered in future.
In such case an user of a BApp is not required to hold an amount of BLN to have write access to the blockchain.
The transaction fee is paid in the BApp token instead.
The protocol permitts inclusion of a transaction without fee into a new block by the stakers.
