---
lastmod: 2019-12-31
title: Deprecated HTTP API version 1
---

HTTP API version 1 has been deprecated and will be removed in future.

New code should use [API version 2](apiv2.html).

### Changes in version 2

POST methods use Content-Type: application/x-www-form-urlencoded for parameters.

JSON responses use Content-Type: application/json.

blockdb/get: changed format of returned data.

transaction data: type moved to data array, blockHash renamed to referenceChain.

transfer, burn, lease, cancellease: return HTTP status 400 if tx was rejected.

addpeer: returns boolean.

disconnectpeer: returns boolean, address and port changed to peerId.

Renamed functions |
--- | ---
peerinfo | peers
nodeinfo | node
blockdb/get | block
blockdb/getblockhash | blockhash
blockdb/getblockindex | blockindex
ledger/get | account
account/generate | generateaccount
address/info | address
mnemonic/info | mnemonic
transaction/raw/send | sendrawtransaction
walletdb/getwallet | wallet/transactions
walletdb/getoutleases | wallet/outleases
walletdb/getsequence | wallet/sequence
walletdb/gettransaction | wallet/transaction
walletdb/getconfirmations | wallet/confirmations
staker/start | startstaking
staker/stop | stopstaking

WebSocket notifications use JSON protocol.
