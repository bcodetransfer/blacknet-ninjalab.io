---
lastmod: 2020-11-12
title: Multi-signature lock contract
titleOnlyInHead: 1
---

## Multisignature lock contract

Multisignature (also multisig) requires M-of-N keys for an authorization.
In Blacknet, it is implemented as a lock that can be created then spent.
Example application of 2-of-3 multisig is a marketplace: buyer, seller, escrow.

## Replay attack

A replay attack is an attack in which a legitimate data is maliciously repeated.
An adversary could try to resubmit a transaction or a transaction data to the blockchain.
An ordinary transaction signature is commited to the transaction header that cannot be replayed.
A spending multi-signature is commited to the contract id that also cannot be replayed.
For creating multi-signature following data is hashed: sender id, sender sequence number, transaction data index number, transaction data without signatures.

## RPC API

RPC API is not yet implemented.

- Create multisig |
--- | ---

- Spend multisig |
--- | ---
