[![Pipeline Status](https://gitlab.com/blacknet-ninja/blacknet-ninja.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/blacknet-ninja/blacknet-ninja.gitlab.io/-/jobs)

---

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hakyll.
1. Generate the website: `stack exec site build`
1. Preview your project: `stack exec site watch`
1. Add content

Read more at Hakyll's [documentation][].

[install]: https://jaspervdj.be/hakyll/tutorials/01-installation.html
[documentation]: https://jaspervdj.be/hakyll/
