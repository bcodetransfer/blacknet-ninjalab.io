---
lastmod: 2020-08-18
title: Documentation HTTP API
---

HTTP API version 2

API web-server listens on [http://localhost:8283/](http://localhost:8283/).
It can be configured in file `rpc.conf`.

Path of all methods is prefixed with `/api/v2`

Encoding of values |
--- | ---
amount | Satoshi (100000000 equals to 1 coin)
data | JSON
hash | HEX-string
boolean | String "true" or "false"
string | UTF-8 string

Errors are returned with HTTP status 400 Bad Request.

Example of GET request: [http://localhost:8283/api/v2/node](http://localhost:8283/api/v2/node)

## Node API

- Node information |
--- | ---
Type | GET
Path | /node
Returns data about this node.

- Peers information |
--- | ---
Type | GET
Path | /peers
Returns data about each connected network node.

- Get block |
--- | ---
Type | GET
Path | /block/{hash}/{txdetail?}
Parameters |
hash | hash of block
txdetail | true to include transaction data (optional boolean, default false)
Returns data of a block with given block-hash.

- Get block index |
--- | ---
Type | GET
Path | /blockindex/{hash}
Parameters |
hash | hash of block
Returns index of a block with given block-hash.

- Get block hash |
--- | ---
Type | GET
Path | /blockhash/{height}
Parameters |
height | height of block
This method may be slow. Returns hash of a block with given block-height.

- Ledger information |
--- | ---
Type | GET
Path | /ledger
Returns data about current state of ledger.

- Account information |
--- | ---
Type | GET
Path | /account/{address}/{confirmations?}
Parameters |
address | address of account
confirmations | number of confirmations for confirmed balance (optional, default 10)
Returns data of an account with given address.

- Transaction pool info |
--- | ---
Type | GET
Path | /txpool
Returns data about memory pool of transactions.

- Generate new account |
--- | ---
Type | GET
Path | /generateaccount
Returns data of new account. Save mnemonic before using account.

- Address information |
--- | ---
Type | GET
Path | /address/{address}
Parameters |
address | address of account
Returns data about address: public key.

- Mnemonic information |
--- | ---
Type | POST
Path | /mnemonic
Parameters |
mnemonic | mnemonic of account
Returns data about mnemonic: address and public key.

- Send raw transaction |
--- | ---
Type | GET
Path | /sendrawtransaction/{hex}
Parameters |
hex | HEX-encoded transaction
Returns transaction hash.

<!---
- Decrypt message |
--- | ---
Type | POST
Path | /decryptmessage
Parameters |
mnemonic | mnemonic of receiver
from | address of sender
message | encrypted message
Returns string.
-->

- Add peer |
--- | ---
Type | GET
Path | /addpeer/{address}/{port?}
Parameters |
address | network address
port | network port (optional, default 28453)
force | force connection (optional boolean, default false)
Returns boolean.

- Disconnect peer |
--- | ---
Type | GET
Path | /disconnectpeer/{id}
Parameters |
id | peer id
Returns boolean.

- Start staking |
--- | ---
Type | POST
Path | /startstaking
Parameters |
mnemonic | mnemonic of staking account
Returns boolean.

- Stop staking |
--- | ---
Type | POST
Path | /stopstaking
Parameters |
mnemonic | mnemonic of staking account
Returns boolean.

- Status of staking |
--- | ---
Type | POST
Path | /isstaking
Parameters |
mnemonic | mnemonic of staking account
Returns boolean.

## Wallet API

- Transfer |
--- | ---
Type | POST
Path | /transfer
Parameters |
mnemonic | mnemonic of wallet
fee | transaction fee
amount | amount to transfer
to | address of receiving account
message | payment id (optional)
encrypted | 0 for plain text message (optional, default 0)
Returns transaction hash.

- Create proof-of-burn |
--- | ---
Type | POST
Path | /burn
Parameters |
mnemonic | mnemonic of wallet
fee | transaction fee
amount | amount to burn
message | HEX-encoded message (optional)
Returns transaction hash.

- Create lease contract |
--- | ---
Type | POST
Path | /lease
Parameters |
mnemonic | mnemonic of wallet
fee | transaction fee
amount | amount to lease
to | address of receiving account
Returns transaction hash.

- Cancel lease contract |
--- | ---
Type | POST
Path | /cancellease
Parameters |
mnemonic | mnemonic of wallet
fee | transaction fee
amount | leased amount
to | address of receiving account
height | height of lease
Returns transaction hash.

- Get wallet transactions |
--- | ---
Type | GET
Path | /wallet/{address}/transactions
Parameters |
address | address of wallet
Returns data of all transactions in wallet.

- Get transaction |
--- | ---
Type | GET
Path | /wallet/{address}/transaction/{hash}/{raw?}
Parameters |
address | address of wallet
hash | hash of transaction
raw | true to return HEX (optional boolean, default false)
Returns data of transaction.

- Get confirmations |
--- | ---
Type | GET
Path | /wallet/{address}/confirmations/{hash}
Parameters |
address | address of wallet
hash | hash of transaction
Returns number of confirmations for transaction.

- Get outgoing leases |
--- | ---
Type | GET
Path | /wallet/{address}/outleases
Parameters |
address | address of wallet
Returns list of outgoing leases.

- Get sequence number |
--- | ---
Type | GET
Path | /wallet/{address}/sequence
Parameters |
address | address of wallet
Returns sequence number of wallet.

- Get reference chain |
--- | ---
Type | GET
Path | /wallet/{address}/referencechain
Parameters |
address | address of wallet
Returns reference chain of wallet.

## Notifications

The web-server accepts WebSocket connections on path `/websocket`

Requests and responses are JSON objects.

- New block

`{ command: "subscribe", route: "block" }`

- New transaction in tx pool

`{ command: "subscribe", route: "txpool" }`

- New transaction in wallet

`{ command: "subscribe", route: "wallet", address: "$address" }`
